window.addEventListener('DOMContentLoaded', async () => {
  await getConferences()
 });


 function createCard(name, description, pictureUrl, start, end, locationName) {
     const formattedStart = new Date(start).toLocaleDateString()
     const formattedEnd = new Date(end).toLocaleDateString()
     return `
       <div class="card mb-4 shadow-lg">
         <img alt="..." src="${pictureUrl}" class="card-img-top">
         <div class="card-body">
           <h5 class="card-title">${name}</h5>
           <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
           <p class="card-text">${description}</p>
         </div>
         <div class="card-footer">
             ${formattedStart} - ${formattedEnd}
          </div>
       </div>
     `;
   }

   function createHorizontalCard(name, description, pictureUrl, start, end, locationName) {

     const formattedStart = new Date(start).toLocaleDateString()
     const formattedEnd = new Date(end).toLocaleDateString()
     return `
     <div class="col">
       <div class="card mb-4 shadow">
         <img alt="..." src="${pictureUrl}" class="card-img-top">
         <div class="card-body">
           <h5 class="card-title">${name}</h5>
           <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
           <p class="card-text">${description}</p>
         </div>
         <div class="card-footer">
             ${formattedStart}-${formattedEnd}
         </div>
       </div>
     </div>
     `;
   }


 async function getConferences() {
     try {

         const url = 'http://localhost:8000/api/conferences/';
         const response = await fetch(url);

         if (!response.ok){
             return alert(
                 `${response.status}: ${response.url} ${response.statusText}`
               );
         } else {
             const data = await response.json()
             console.log('data', data)
             const conferences = data.conferences;
             for (let i = 0; i < conferences.length; i++){
                 const conference = conferences[i];

                 const detailUrl = `http://localhost:8000${conference.href}`;
                 const detailResponse = await fetch(detailUrl);
                 if (!detailResponse.ok) {
                   return alert(
                     `${response.status}: ${response.url} ${response.statusText}`
                   );
                 } else {

                     const details = await detailResponse.json();
                     console.log('details', details)
                     const title = details.conference.name;
                     const description = details.conference.description;
                     const pictureUrl = details.conference.location.picture_url;
                     const starts = details.conference.starts
                     const ends = details.conference.ends
                     const locationName = details.conference.location.name


                     if (i < 2){
                         const html = createCard(title, description, pictureUrl, starts, ends, locationName);
                         const column = document.querySelector('.col');
                         column.innerHTML += html
                     } else {
                         const html = createHorizontalCard(title, description, pictureUrl, starts, ends, locationName)
                         const cardRow = document.getElementById("horizontal-conferences")
                         cardRow.innerHTML += html
                     }
                 }
             }

         }


     } catch (err){
         return alert(
             `${err?.message || "There was an unknown error that occurred"}`
           );
     }
 }


 function alert(message) {
   const alertPlaceholder = document.getElementById("liveAlertPlaceholder");
   const wrapper = document.createElement("div");
   wrapper.innerHTML =
     '<div class="alert alert-danger' +
     ' alert-dismissible" role="alert">' +
     message +
     '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>';

   alertPlaceholder.append(wrapper);
 }

 /// const categoriesUrl = "http://localhost:8000/api/conferences/"
 /// const cetegoriesResponse = await fetch(categoriesUrl)
 /// if (categoriesResponse.ok){
/// const data = await categoriesResponse.json()
///console.log(data)
/// const selectTag = document.getElementById('categoryId')  THIS IS THE TAG AND ID OF THE BUTTON YOU WANT TO LIST THE CONFERENCES
/// for(let conference of data.conference.slice(0,10)){
/// const option = document.createElement('option')
/// option.value = conference.id
/// option.innerHTML = conference.name
/// selectTag.appendChild(option)
/// console.log(option)
//}
///}else{
  ///console.error("Ain't no sunshine when she's gone")
 ///}


 ///const form = document.getElementById("create-clue-form") PROBABLY CONFERENCE DETAILS
 ///console.log(form)
 ///form.addEventListener('submit', async event => {
  ///event.preventDefault()
  ///const formData = new FormData(form)
  ///const dataObject = Object.fromEntries(formData)
  /// const fetchOptions = {
    /// method: 'POST';
    ///body: JSON.stringify(dataObject)
    ///headers: {
    ///'Content-Type': 'application/json'
 ///}
 ///}
 ///const newUrl = ''   PROBABLY SUBMITTED CONFERENCE URL
 /// const newConferenceResponse = await fetch(newUrl, fetchOptions)
 /// console.log(newConferenceResponse)
 ///if(newClueResponse.ok){
  ///const newConference = await newConferenceResponse.json()
  ///const name = newConference.name
  ///const description = newConference.description
  ///const location = newConference.location
 /// const html = createCard(name,description,location)
 /// conferenceDiv.innerHTML= html
 ///} else{
  ///console.error("There is a house, in New 'rleans")
 ///}
//})
